package dtwtest01;

/**
 *
 * @author krato
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        Test test = new Test();
        test.prepare();
        long start = System.nanoTime();
        int offset = test.findOffset();
        long time = System.nanoTime() - start;
        System.out.printf("Offset: %d%n", offset);
        System.out.printf("Took: %.9f ms%n", time / 1e6);

    }
}
