package dtwtest01;

/**
 *
 * @author krato
 */
public class Test {

    private static int dataSize = 100 * 1000;
    private static int patternSize = 100;
    private static int patternOffset = dataSize - patternSize;
    private float[] x = new float[dataSize];

    public void prepare() {
        for (int i = 0; i < patternOffset; i++) {
            x[i] = i;
        }
        for (int i = 0; i < patternSize; i++) {
            x[i + patternOffset] = i;
        }
    }

    float min(float v1, float v2) {
        if (v1 < v2) {
            return v1;
        }
        return v2;
    }

    float[][] m1 = new float[patternSize][patternSize];
    float[][] m2 = new float[patternSize][patternSize];

    final float dtwDistance(final float[] p1, final float[] p2) {
        int i, j;
        for (i = 0; i < patternSize; i++) {
            for (j = 0; j < patternSize; j++) {
                if (p1[i] > p2[j]) {
                    m1[i][j] = p1[i] - p2[j];
                } else {
                    m1[i][j] = p2[j] - p1[i];
                }
            }
        }
        for (i = 0; i < patternSize; i++) {
            m2[i][0] = m1[i][0];
        }
        for (i = 1; i < patternSize; i++) {
            for (j = 0; j < patternSize; j++) {
                if (j == 0) {
                    m2[i][j] = m2[i - 1][j];
                } else {
                    m2[i][j] = min(min(m2[i - 1][j], m2[i][j - 1]), m2[i - 1][j - 1]);
                }
                m2[i][j] += m1[i][j];
            }
        }
        /*
         for (i = 1; i < patternSize; i++) {
         for (j = 0; j < patternSize; j++) {
         printf("%f ", m1[i][j]);
         }
         printf("\n");
         }
         exit(1);
         */
        return m2[patternSize - 1][patternSize - 1];
    }

    int findOffset() {

        float[] p1 = new float[patternSize];
        float[] p2 = new float[patternSize];
        float distance;
        int i, j;

        for (i = 0; i < patternSize; i++) {
            p1[i] = x[i];
        }

        for (i = patternSize; i <= patternOffset; i++) {
            for (j = 0; j < patternSize; j++) {
                p2[j] = x[i + j];
            }
            distance = dtwDistance(p1, p2);
            if (distance == 0) {
                return i;
            }
        }

        return -1;
    }

}
