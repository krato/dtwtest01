#include <stdio.h>
#include <time.h>

#define dataSize 100 * 1000
#define patternSize 100
#define patternOffset dataSize - patternSize
float x[dataSize];

void prepare() {
    int i;
    for (i = 0; i < patternOffset; i++) {
        x[i] = i;
    }
    for (i = 0; i < patternSize; i++) {
        x[i + patternOffset] = i;
    }
}

float min(float v1, float v2) {
    if (v1 < v2) return v1;
    return v2;
}

float m1[patternSize][patternSize];
float m2[patternSize][patternSize];

float dtwDistance(float* p1, float* p2) {
    int i, j;
    for (i = 0; i < patternSize; i++) {
        for (j = 0; j < patternSize; j++) {
            if (p1[i] > p2[j]) m1[i][j] = p1[i] - p2[j];
            else m1[i][j] = p2[j] - p1[i];
        }
    }
    for (i = 0; i < patternSize; i++) {
        m2[i][0] = m1[i][0];
    }
    for (i = 1; i < patternSize; i++) {
        for (j = 0; j < patternSize; j++) {
            if (j == 0) {
                m2[i][j] = m2[i - 1][j];
            } else {
                m2[i][j] = min(min(m2[i - 1][j], m2[i][j - 1]), m2[i - 1][j - 1]);
            }
            m2[i][j] += m1[i][j];
        }
    }
    /*
            for (i = 1; i < patternSize; i++) {
                for (j = 0; j < patternSize; j++) {
                    printf("%f ", m1[i][j]);
                }
                printf("\n");
            }
            exit(1);
     */
    return m2[patternSize - 1][patternSize - 1];
}

int findOffset() {

    float p1[patternSize];
    float p2[patternSize];
    float distance;
    int i, j;

    for (i = 0; i < patternSize; i++) {
        p1[i] = x[i];
    }

    for (i = patternSize; i <= patternOffset; i++) {
        for (j = 0; j < patternSize; j++) {
            p2[j] = x[i + j];
        }
        distance = dtwDistance(p1, p2);
        if (distance == 0) {
            return i;
        }
    }

    return -1;
}

int main() {
    int offset;
    float time1;
    float time2;
    prepare();
    time1 = (float) clock() / CLOCKS_PER_SEC;
    offset = findOffset();
    time2 = (float) clock() / CLOCKS_PER_SEC;
    printf("Offset: %d\n", offset);
    printf("Took: %.9f ms\n", (time2 - time1) * 1000);
    return 0; /* success */
}
