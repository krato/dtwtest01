<?php

/**
 *
 * @author krato
 */
class Test {

    const dataSize = 1000;
    const patternSize = 100;
    const patternOffset = 999;

    private $x;

    public function prepare() {
        $x = &$this->x;
        $m1 = &$this->m1;
        $m2 = &$this->m2;

        for ($i = 0; $i < self::patternOffset; $i++) {
            $x[$i] = $i;
        }
        for ($i = 0; $i < self::patternSize; $i++) {
            $x[$i + self::patternOffset] = $i;
        }
        for ($i = 0; $i < self::patternSize; $i++) {
            for ($j = 0; $j < self::patternSize; $j++) {
                $m1[$i][$j] = 0;
                $m2[$i][$j] = 0;
            }
        }
    }

    private static function min(&$v1, &$v2) {
        if ($v1 < $v2) {
            return $v1;
        }
        return $v2;
    }

    private $m1;
    private $m2;

    private function dtwDistance(&$p1, &$p2) {
        $m1 = &$this->m1;
        $m2 = &$this->m2;
        
        for ($i = 0; $i < self::patternSize; $i++) {
            for ($j = 0; $j < self::patternSize; $j++) {
                if ($p1[$i] > $p2[$j]) {
                    $m1[$i][$j] = $p1[$i] - $p2[$j];
                } else {
                    $m1[$i][$j] = $p2[$j] - $p1[$i];
                }
            }
        }
        for ($i = 0; $i < self::patternSize; $i++) {
            $m2[$i][0] = $m1[$i][0];
        }
        for ($i = 1; $i < self::patternSize; $i++) {
            for ($j = 0; $j < self::patternSize; $j++) {
                if ($j == 0) {
                    $m2[$i][$j] = $m2[$i - 1][$j];
                } else {
                    $m2[$i][$j] = self::min(self::min($m2[$i - 1][$j], $m2[$i][$j - 1]), $m2[$i - 1][$j - 1]);
                }
                $m2[$i][$j] += $m1[$i][$j];
            }
        }

        /*
        for ($i = 1; $i < self::patternSize; $i++) {
            for ($j = 0; $j < self::patternSize; $j++) {
                printf("%f ", $m1[$i][$j]);
            }
            printf("\n");
        }
        exit(1);
         */

        return $m2[self::patternSize - 1][self::patternSize - 1];
    }

    public function findOffset() {

        $x = &$this->x;

        for ($i = 0; $i < self::patternSize; $i++) {
            $p1[$i] = $x[$i];
        }

        for ($i = self::patternSize; $i <= self::patternOffset; $i++) {
            for ($j = 0; $j < self::patternSize; $j++) {
                $p2[$j] = $x[$i + $j];
            }
            $distance = $this->dtwDistance($p1, $p2);
            if ($distance == 0) {
                return $i;
            }
        }

        return -1;
    }

}
