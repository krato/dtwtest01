<?php

require_once 'Test.php';

/**
 *
 * @author krato
 */
class Main {

    private static function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    /**
     * @param args the command line arguments
     */
    public static function run(array $args) {
        $test = new Test();
        $test->prepare();
        $start = self::microtime_float();
        $offset = $test->findOffset();
        $time = self::microtime_float() - $start;
        printf("Offset: %d \n", $offset);
        printf("Took: %.9f ms \n", $time * 1000);
    }

}

Main::run(array());
